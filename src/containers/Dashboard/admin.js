import React from 'react';
import {connect} from "react-redux";
import {getBooks, addBook, deleteBook} from "./actions";
import {IntlProvider, FormattedMessage} from 'react-intl';
import BookItem from "../../components/Book/bookItem";
import './dashboard.css'

import messages from './messages';
import {Button, Col, Row} from "react-bootstrap";
import Form from "react-bootstrap/Form";


class DashboardAdmin extends React.Component {
    constructor(props) {
        super(props);
        this.props.getBooks();
        this.state = {
            inputTitle: "",
            inputAuthor: "",
            inputYear: ""
        }

        this.handleInputChange = this.handleInputChange.bind(this);
        this.doDeleteBook = this.doDeleteBook.bind(this);
    }

    doLogout(event, props) {
        localStorage.clear();
        this.props.history.push("/");
    }

    doAddBook(event, props) {
        addBook(this.state.inputTitle, this.state.inputAuthor, this.state.inputYear)
            .then(() => props.getBooks())
        this.setState({
            inputTitle: "",
            inputAuthor: "",
            inputYear: ""
        })
    }

    doDeleteBook(event, props, id) {
        deleteBook(id).then(() => props.getBooks())

    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState({[name]: value});
    }

    render() {
        let booksList = "";
        if (this.props.bookList !== null) {
            booksList = this.props.bookList.map(book =>
                <li key={book.id} className="list-group-item">
                    <Row>
                        <BookItem book={book} key={book.id}/>
                        <Col xs="auto">
                            <Button variant="danger" key={book.id} className="float-right"
                                    onClick={(e) => this.doDeleteBook(e, this.props, book.id, this.state)}>
                                <FormattedMessage id="delete"/>
                            </Button>
                        </Col>
                    </Row>
                </li>)
        }

        return (
            <IntlProvider locale={navigator.language}
                          messages={messages[navigator.language.substr(0, 2)]}>
                <Col className="container-fluid min-vh-100" id="login-page">
                    <Col className="row min-vh-100 justify-content-center align-content-center">
                        <Col className="col-md-10 login-container">
                            <ul className="list-group" id="booksList">
                                {booksList}
                            </ul>
                            <Row className="mt-5">
                                <Col>
                                    <div className="form-group">
                                        <label htmlFor="input-title"><FormattedMessage id="title"/>:</label>
                                        <FormattedMessage id="title">
                                            {placeholder =>
                                                <Form.Control type="text" id="inputTitle" placeholder={placeholder}
                                                              value={this.state.inputTitle}
                                                              onChange={this.handleInputChange}
                                                              name="inputTitle" required/>}
                                        </FormattedMessage>
                                    </div>
                                </Col>
                                <Col>
                                    <div className="form-group">
                                        <label htmlFor="input-author"><FormattedMessage id="author"/>:</label>
                                        <FormattedMessage id="author">
                                            {placeholder =>
                                                <Form.Control type="text" id="inputAuthor" placeholder={placeholder}
                                                              value={this.state.inputAuthor}
                                                              onChange={this.handleInputChange}
                                                              name="inputAuthor" required/>}
                                        </FormattedMessage>
                                    </div>
                                </Col>
                                <Col>
                                    <div className="form-group">
                                        <label htmlFor="input-year"><FormattedMessage id="year"/>:</label>
                                        <FormattedMessage id="year">
                                            {placeholder =>
                                                <Form.Control type="number" id="inputYear" min="100" name="inputYear"
                                                              max={new Date().getFullYear()} placeholder={placeholder}
                                                              value={this.state.inputYear}
                                                              onChange={this.handleInputChange}
                                                              required/>}
                                        </FormattedMessage>
                                    </div>
                                </Col>
                                <Col className="pt-4 mt-2">
                                    <button className="btn btn-success col"
                                            onClick={(e) => this.doAddBook(e, this.props)}>
                                        <FormattedMessage id="add"/>
                                    </button>
                                </Col>
                            </Row>
                            <Row className="mt-5">
                                <button className="btn btn-danger col-12" onClick={(e) => this.doLogout(e, this.props)}>
                                    <FormattedMessage id="logout"/>
                                </button>
                            </Row>
                        </Col>
                    </Col>
                </Col>
            </IntlProvider>
        );
    }
}


const mapStateToProps = state => {
    return {
        bookList: state.dashboard.bookList,
    }
}


export default connect(mapStateToProps, {getBooks})(DashboardAdmin);
