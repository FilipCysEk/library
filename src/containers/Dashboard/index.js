import React from 'react';
import {connect} from "react-redux";
import {getBooks} from "./actions";
import {IntlProvider, FormattedMessage} from 'react-intl';
import BookItem from "../../components/Book/bookItem";
import './dashboard.css'

import messages from './messages';
import {Row} from "react-bootstrap";


class Dashboard extends React.Component {
    constructor(props) {
        super(props);
        this.props.getBooks();
    }

    doLogout(event, props) {
        localStorage.clear();
        this.props.history.push("/");
    }

    render() {
        let booksList = "";
        if (this.props.bookList !== null) {
            booksList = this.props.bookList.map(book =>
                <li key={book.id} className="list-group-item"><BookItem book={book} key={book.id}/></li>)
        }

        return (
            <IntlProvider locale={navigator.language}
                          messages={messages[navigator.language.substr(0, 2)]}>
                <div className="container-fluid min-vh-100" id="login-page">
                    <div className="row min-vh-100 justify-content-center align-content-center">
                        <div className="col-md-10 login-container">
                            <ul className="list-group" id="booksList">
                                {booksList}
                            </ul>
                            <Row className="mt-5">
                                <button className="btn btn-danger col-12" onClick={(e) => this.doLogout(e, this.props)}>
                                    <FormattedMessage id="logout"/>
                                </button>
                            </Row>
                        </div>
                    </div>
                </div>
            </IntlProvider>
        );
    }
}


const mapStateToProps = state => {
    return {
        bookList: state.dashboard.bookList,
    }
}


export default connect(mapStateToProps, {getBooks})(Dashboard);
