import {serverUrl} from '../../const'

export const getBooks = () => dispatch => {
    const base64Credentials = localStorage.getItem("auth")

    fetch(serverUrl + "dashboard",
        {
            method: 'GET',
            headers: {
                'Access-Control-Allow-Credentials': true,
                'Access-Control-Allow-Origin': true,
                'Authorization': 'Basic ' + base64Credentials,
            }
        }).then(response => response.json())
        .then(json =>
            dispatch({
                type: 'UPDATE_BOOK_LIST',
                bookList: json,
            })
        )
        .catch((response) => {
            console.log("Error")
        });
}

export const addBook = (title, author, year) => {
    const base64Credentials = localStorage.getItem("auth")
    return fetch(serverUrl + "dashboard",
        {
            method: 'POST',
            headers: {
                'Access-Control-Allow-Credentials': true,
                'Access-Control-Allow-Origin': true,
                'Authorization': 'Basic ' + base64Credentials,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                'title':title,
                'author':author,
                'year':year
            })
        }).then(response => response.json())
        .then(json =>
            console.log("Added book", json)
        )
        .catch((response) => {
            console.log("Error")
        });
}

export const deleteBook = (id) => {
    const base64Credentials = localStorage.getItem("auth")
    return fetch(serverUrl + "dashboard/" + id,
        {
            method: 'DELETE',
            headers: {
                'Access-Control-Allow-Credentials': true,
                'Access-Control-Allow-Origin': true,
                'Authorization': 'Basic ' + base64Credentials,
                'Content-Type': 'application/json',
            },
        }).then(response => response.json())
        .then(json =>
            console.log("Deleted book", json)
        )
        .catch((response) => {
            console.log("Error")
        });
}