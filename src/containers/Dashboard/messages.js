const messages = {
    pl: {
        logout: "Wyloguj",
        title: "Tytuł",
        author: "Autor",
        year: "Rok",
        add: "Dodaj",
        delete: "Usuń ksiazkę",
    },
    en: {
        logout: "Logout!",
        title: "Title",
        author: "Author",
        year: "Year",
        add: "Add book",
        delete: "Delete book",
    }
};

export default messages