import React from 'react';
import {Link, withRouter} from "react-router-dom";


class NotAuth extends React.Component{

    render() {
        return (
            <div className="container-fluid h-100" id="login-page">
                <div className="row h-100 justify-content-center align-content-center">
                    Brak uprawnien!!
                    <Link to="/">Wroć do strony glownej</Link>
                </div>
            </div>
        );
    }
}


export default withRouter(NotAuth);
