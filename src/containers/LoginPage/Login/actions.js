import {serverUrl} from '../../../const'

export const setPushMessage = (num) => dispatch => {
    dispatch({
        type: 'SET_PUSHMESSAGE',
        pushMessage: num,
    })
}

export const loginRequest = (login, password) => dispatch => {
    const base64Credentials = btoa(login + ":" + password);

    return fetch(serverUrl + "login",
        {
            method: 'GET',
            headers: {
                'Access-Control-Allow-Credentials': true,
                'Access-Control-Allow-Origin': true,
                'Authorization': 'Basic ' + base64Credentials,
            }
        }).then((response) => {
        if (response.status === 401)
            return 1;
        else if (response.status === 200) {
            return response.json().then((json) => {
                dispatch({
                    type: 'SET_USER_ROLE',
                    userRole: json[0]["role"],
                })
                localStorage.setItem('role', json[0]["role"])
                localStorage.setItem("auth", base64Credentials);
                return 0;
            });
        } else return 2;
    })
        //     .then((json) => {
        //
        //     return resptype;
        // })
        .catch((response) => {
            return 3;
        });

    // let data = response.json()
    // dispatch({
    //     type:'SET_USER_ROLE',
    //     userRole: data[0]["role"],
    // })
    //
    // if(response === 3) return 3;
    // else if (response.status === 401)
    //     return 1;
    // else if (response.status === 200) {
    //     let data = response.json()
    //     return data[0]["role"];
    // }
    // else return 2;
};

export const getUserRole = () => dispatch => {
        const base64Credentials = localStorage.getItem("auth")

        return fetch(serverUrl + "login",
            {
                method: 'GET',
                headers: {
                    'Access-Control-Allow-Credentials': true,
                    'Access-Control-Allow-Origin': true,
                    'Authorization': 'Basic ' + base64Credentials,
                }
            }).then((response) => {
                if (response.status === 200) {
                    response.json().then((json) => {
                        dispatch({
                            type: 'SET_USER_ROLE',
                            userRole: json[0]["role"],
                        })
                    });
                    return true;
                } else {
                    return false;
                }
            })
            .catch((response) => {
                return false;
            });
    }
;