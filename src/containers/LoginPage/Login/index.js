import React from 'react';
import './index.css';
import {connect} from 'react-redux'
import LoginPageComp from './../../../components/LoginPage/index.js'
import LoginPageHeaderComp from "../../../components/loginPageHeader";
import {loginRequest, setPushMessage, getUserRole} from './actions';
import Auth from "../../../Auth";


class LoginPage extends React.Component {
    constructor(props) {
        super(props);
        if(this.checkUserIsLogged()){
            this.redirectToDashboard()
        }
    }


    doOnsubmit(event, props) {
        event.preventDefault();
        props.loginRequest(event.target.elements.formLogin.value, event.target.elements.formPassword.value).then(
            (result) => {
                if (result !== 0) {
                    props.setPushMessage(result);
                } else {
                    props.setPushMessage(0);
                    this.redirectToDashboard()
                }
            });
    }

    checkUserIsLogged(){
        let auth = new Auth();
        if(auth.checkLogin()){
            return true;
        } else return false;
    }

    redirectToDashboard(){
        if (localStorage.getItem('role') === "ROLE_ADMIN") {
            this.props.history.push('/dashboardAdmin')
            //return <Redirect to='/dashboardAdmin'/>;
        //} else if(this.props.userRole === "ROLE_USER"){
        } else{
            this.props.history.push('/dashboard')
            //return <Redirect to='/dashboard'/>;
        }
    }

    render() {
        // if(this.checkUserIsLogged()){
        //     return this.redirectToDashboard();
        //     console.log(this.props)
        // }

        return (
            <div className="container-fluid min-vh-100" id="login-page">
                <div className="row min-vh-100 justify-content-center align-content-center">
                    <div className="col-md-4 login-container">
                        <LoginPageHeaderComp/>
                        <LoginPageComp onSubmit={(values) => this.doOnsubmit(values, this.props)}/>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        pushMessage: state.loginPage.pushMessage,
        userRole: state.loginPage.userRole,
    }
}


export default connect(mapStateToProps, {setPushMessage, loginRequest, getUserRole})(LoginPage);
