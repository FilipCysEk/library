export const initialState = {
    pushMessage: 0,
    userRole: null,
    auth: null,
}

const loginPageReducer = (state = initialState, action) => {
    switch(action.type){
        case 'SET_PUSHMESSAGE':
            return {...state, pushMessage: action.pushMessage,}

        case 'SET_USER_ROLE':
            return {...state, userRole: action.userRole,}

        default:
            return state
    }
}

export default loginPageReducer