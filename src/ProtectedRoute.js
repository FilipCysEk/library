import React from 'react';
import {Route, Redirect} from 'react-router-dom';
import Auth from "./Auth";


function ProtectedRoute({component: Component, roles = [], ...rest}) {
    const auth = new Auth();
    return (
        <Route  {...rest}
        render={props => {
            if(auth.userCanAccess(roles)) {
                return <Component {...props}/>
            } else {
                return <Redirect to="/NotAuth"/>
            }
        }}
        />
    );
}

export default ProtectedRoute;