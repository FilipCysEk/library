import React from 'react';
import {IntlProvider, FormattedMessage} from 'react-intl';

import messages from './messages';

const LoginPageHeaderComp = () => {


    return (
        <IntlProvider locale={navigator.language}
                      messages={messages[navigator.language.substr(0, 2)]}>
            <div className="row-cols-1">
                <div className="col">
                    <h1 className="text-center">
                        <FormattedMessage id="welcome"/>
                    </h1>
                    <h3 className="text-center">
                        <FormattedMessage id="welcomeSecondLine"/>
                    </h3>
                </div>
            </div>
        </IntlProvider>
    );
};

export default LoginPageHeaderComp;