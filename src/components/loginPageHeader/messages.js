const messages = {
    pl: {
        welcome: "Witaj na stronie Biblioteki!",
        welcomeSecondLine: "Zaloguj!"
    },
    en: {
        welcome: "Witaj na stronie Biblioteki!",
        welcomeSecondLine: "Zaloguj!"
    }
};

export default messages