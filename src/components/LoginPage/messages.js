const messages = {
    pl: {
        login: "Login",
        loginPlaceholder: "login",
        loginAction: "Zaloguj!",
        password: "Hasło",
        passwordPlaceholder: "Hasło",
        badPassword: "Złe hasło lub login!",
        serverError: "Błąd serwera",
        connectionError: "Błąd połączenia",
    },
    en: {
        login: "Login",
        loginPlaceholder: "Login",
        loginAction: "Login",
        password: "Password",
        passwordPlaceholder: "Password",
        badPassword: "Złe hasło lub login!",
        serverError: "Błąd serwera",
        connectionError: "Błąd połączenia",
    }
};

export default messages