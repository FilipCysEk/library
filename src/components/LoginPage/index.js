import React from 'react';
import {connect} from 'react-redux'
import {IntlProvider, FormattedMessage} from 'react-intl';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';


import messages from './messages';

const LoginPageComp = ({onSubmit, pushMessage}) => {
    let errMessageId = 'badPassword';
    let isInvalidFlg = false;
    if(pushMessage === 1){
        errMessageId = 'badPassword';
        isInvalidFlg = true;
    } else if(pushMessage === 2){
        errMessageId = 'serverError';
        isInvalidFlg = true;
    } else if(pushMessage === 3){
        errMessageId = 'connectionError';
        isInvalidFlg = true;
    }

    return (
        <IntlProvider locale={navigator.language}
                      messages={messages[navigator.language.substr(0, 2)]}>
            <Form onSubmit={onSubmit}>
                <Form.Group controlId="formLogin">
                    <Form.Label><FormattedMessage id="login"/></Form.Label>
                    <FormattedMessage id="loginPlaceholder">
                        {placeholder => <Form.Control type="text" isInvalid={isInvalidFlg}
                                                      placeholder={placeholder}/>}
                    </FormattedMessage>
                </Form.Group>
                <Form.Group controlId="formPassword">
                    <Form.Label><FormattedMessage id="password"/></Form.Label>
                    <FormattedMessage id="passwordPlaceholder">
                        {placeholder => <Form.Control type="password"  isInvalid={isInvalidFlg}
                                                      placeholder={placeholder}/>}
                    </FormattedMessage>
                    <Form.Control.Feedback type="invalid">
                        <FormattedMessage id={errMessageId}/>
                    </Form.Control.Feedback>
                </Form.Group>
                <Form.Group>
                    <Button variant="success" type="submit">
                        <FormattedMessage id="loginAction"/>
                    </Button>
                </Form.Group>
            </Form>
        </IntlProvider>
    );
};

const mapStateToProps = state => ({
    pushMessage: state.loginPage.pushMessage

})

export default connect(mapStateToProps)(LoginPageComp);