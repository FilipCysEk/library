//import store from './store'

class Auth {
    checkLogin() {
        return !(localStorage.getItem('auth') === undefined || localStorage.getItem("auth") === ""
        || localStorage.getItem('auth') === null);
    }

    checkRights(allowedUserType) {
        //let state = store.getState();

        if(allowedUserType.length === 0)
            return true;

        for(let i = 0; i < allowedUserType.length; i++){
            if(localStorage.getItem('role') === allowedUserType[i])
                return true;
        }
        return false;
    }

    userCanAccess(allowedUserType) {
        if(this.checkLogin()){
            if(this.checkRights(allowedUserType))
                return true;
        }

        return false;
    }
}
export default Auth

//
// const mapStateToProps = state => {
//     return {
//         userRole: state.loginPage.userRole,
//     }
// }
//
//
// export default connect(mapStateToProps)(Auth);