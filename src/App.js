import React from 'react';
import './css/App.css';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import LoginPage from "./containers/LoginPage/Login";
import Dashboard from "./containers/Dashboard";
import ProtectedRoute from "./ProtectedRoute";
import NotAuth from "./containers/ErrorPages/NotAuth";
import DashboardAdmin from "./containers/Dashboard/admin";

function App() {

    return (
        <Router>
            <Switch>
                <Route path="/login" component={LoginPage}/>
                <ProtectedRoute path="/dashboard" component={Dashboard} />
                <ProtectedRoute path="/dashboardAdmin" roles ={["ROLE_ADMIN"]} component={DashboardAdmin} />

                //Default page
                <Route path="/" exact component={LoginPage}/>
                <Route path="/NotAuth" exact component={NotAuth}/>
                <Route path="*" component={() => "404 not found!"}/>
            </Switch>
        </Router>
    );
}

export default App;
