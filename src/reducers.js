import { combineReducers } from 'redux'
import loginPageReducer from './containers/LoginPage/Login/reducer'
import dashboardReducer from './containers/Dashboard/reducer'

const rootReducer = combineReducers({
    loginPage: loginPageReducer,
    dashboard: dashboardReducer,
});

export default rootReducer